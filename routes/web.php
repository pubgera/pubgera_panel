<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('dashboard');});

Route::get('/edit', function () { return view('pages.TourEdit');});
Route::get('/edit/{tournament_id}', 'TournamentController@getTourDetails');
Route::post('/addprice', 'TournamentController@addPricePool');



Route::get('/create-tournament', function(){return view('pages.CreateTournament');});

Route::post('/addtournament', 'TournamentController@addTournament');
Route::get('/tourlist', 'TournamentController@getAllTour');
Route::get('/users', 'UserController@getAllUsers');