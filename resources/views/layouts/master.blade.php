<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Enlink - Admin Dashboard Template</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/images/logo/favicon.png">

    <!-- page css -->
    <link href="{{ URL::asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/vendors/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet">

    <!-- Made By Chirag -->
    <link href="{{ URL::asset('http://demos.codexworld.com/bootstrap-datetimepicker-add-date-time-picker-input-field/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">


    <!-- Core css -->
    <link href="{{ URL::asset('assets/css/app.min.css') }}" rel="stylesheet">

</head>

  	<body>
    	<div class="app">
	  		<div class="layout">
	    		<!-- Header START -->
	    		@include ('layouts.include.header')
	            <!-- Header END -->

	            <!-- Side Nav START -->
	            @include ('layouts.include.sidebar')
	            <!-- Side Nav END -->

		  		<!-- Page Container START -->
		        <div class="page-container">
		        	<!-- Content Wrapper START -->		        	
	            	<div class="main-content">
	            		@yield('content')
		            </div>		            
			  		<!-- Footer START -->
			        <footer class="footer">
			            <div class="footer-content">
			                <p class="m-b-0">Copyright © {{\Carbon\Carbon::today()->format('Y')}} All rights reserved.</p>
			            </div>
			        </footer>
			        <!-- Footer END -->
		        </div>
		        <!-- Page Container END -->
        	</div>
        </div>



    <!-- Core Vendors JS -->
    <script src="{{URL::asset('assets/js/vendors.min.js')}}"></script>

    <!-- page js -->
    <script src="{{URL::asset('assets/vendors/chartjs/Chart.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/pages/dashboard-default.js')}}"></script>

    <!-- Core JS -->
    <script src="{{URL::asset('assets/js/app.min.js')}}"></script>

    <!-- Datepicker JS -->
    <script src="{{URL::asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>

    <script src="{{URL::asset('assets/vendors/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('assets/vendors/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/pages/datatables.js')}}"></script>
    

    <!-- Moment JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script src="{{URL::asset('assets/js/moment-timezone.js')}}"></script>
    <script src="{{URL::asset('assets/js/moment-timezone-with-data.js')}}"></script>
    

    <!-- Made By Chirag -->
    <script src="{{URL::asset('http://demos.codexworld.com/bootstrap-datetimepicker-add-date-time-picker-input-field/js/bootstrap-datetimepicker.min.js')}}"></script>

    @yield('customJs')

  	</body>
</html>
