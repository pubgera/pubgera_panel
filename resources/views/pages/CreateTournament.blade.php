@extends('layouts.master')

@section('content')

                    <div class="page-header">                        
                        <div class="header-sub-title">
                            <nav class="breadcrumb breadcrumb-dash">
                                <a href="#" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Home</a>
                                <a class="breadcrumb-item" href="#">Creat Tournament</a>
                                <span class="breadcrumb-item active"></span>
                            </nav>
                        </div>
                    </div>
                    @if (Session::has('message'))
                   <div class="alert alert-success" role="alert">
                       {{Session::get('message')}}
                   </div>
                    @endif
                    <div class="card">
                        <div class="card-body">                          
                            <form action="addtournament" method="POST">

                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label control-label">Tournament Title</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="tour_name" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label control-label">Tournament Type</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" name="tour_type">
                                            <option value="Solo">Solo</option>
                                            <option value="Duo">Duo</option>
                                            <option value="Squad">Squad</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label control-label">Tournament Type</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" name="tour_map">
                                            <option value="ERANGLE [TPP]">ERANGLE [TPP]</option>
                                            <option value="SANHOK [TPP]">SANHOK [TPP]</option>
                                            <option value="VIKENDI [TTP]">VIKENDI [TTP]</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label control-label">Set Tournament Date & Time</label>
                                    <div class="col-sm-2">
                                        <div class="input-affix m-b-10">
                                            <i class="prefix-icon anticon anticon-calendar"></i>
                                            <input type="text" name="tour_date" class="form-control datepicker" placeholder="Pick a date">
                                        </div>
                                    </div>
                                    <!-- <div class="col-sm-2">
                                        <div class="input-affix m-b-10">
                                            <i class="prefix-icon anticon anticon"></i>
                                            <input type="text" name="" class="form-control timepicker" placeholder="Pick a date">
                                        </div>
                                    </div> -->
                                </div>                            
                                <!-- <div class="form-group row">
                                    <label class="col-sm-2 col-form-label control-label">Set Tournament Time</label>
                                    <div class="col-sm-6">
                                        <div class="input-affix m-b-10">
                                            <i class="prefix-icon anticon anticon-calendar"></i>
                                            <input type="text" name="tour_date" class="form-control " placeholder="Pick a date">
                                        </div>
                                    </div>
                                </div> -->
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label control-label">Price Pool</label>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control" name="tour_price_pool" placeholder="Enter Price Pool">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label control-label">Set Players</label>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control" name="tour_player" placeholder="Enter Players Limit">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label control-label">Entry Fee</label>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control" name="tour_entry_fee" placeholder="Enter Entry Fee">
                                    </div>
                                </div>                                                            
                                <div class="form-group text-center">
                                    <button class="btn btn-primary">Create Tournament</button>
                                </div>
                            </form>                                                    
                        </div>
                    </div>
                </div>



        </div>
    </div>

@stop
@section('customJs')

<script type="text/javascript">

    $(function() {
      $('.datepicker').datetimepicker({
        format: 'dd-mm-yyyy hh:mm',
        twentyFour: false,

      });
    });


    $('.timepicker').timepicker();
    
</script>


@stop