@extends('layouts.master')

@section('content')

                    <div class="page-header">
                        <h2 class="header-title">Invoice</h2>
                        <div class="header-sub-title">
                            <nav class="breadcrumb breadcrumb-dash">
                                <a href="#" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Home</a>
                                <a class="breadcrumb-item" href="#">Pages</a>
                                <span class="breadcrumb-item active">Edit Tournament</span>
                            </nav>
                        </div>
                    </div>
                    @foreach($GetTourDetails as $data)
                    <div class="container">
                        <div class="card">
                            <div class="card-body">
                                <div id="invoice" class="p-h-30">
                                    <div class="m-t-15 lh-2">
                                        <div class="float-right">
                                            <h2>Tour ID :{{ $data->tournament_id}}</h2>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="row m-t-20 lh-2">
                                        <div class="col-sm-9">
                                            <h3 class="p-l-10 m-t-10">{{ $data->tour_name}}</h3>
                                            <address class="p-l-10 m-t-10">
                                                <span class="font-weight-semibold text-dark">Map: </span>{{ $data->tour_map}}<br>
                                                <span class="font-weight-semibold text-dark">Entry Fee: </span>₹{{ $data->tour_entry_fee}}<br>
                                                <span class="font-weight-semibold text-dark">Status:</span> {{ $data->tour_status}}
                                            </address>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="m-t-80">
                                                <div class="text-dark text-uppercase d-inline-block">
                                                    <span class="font-weight-semibold text-dark">Price Pool:</span></div>
                                                <div class="float-right">₹{{$data->tour_price_pool }}</div>
                                            </div>
                                            <div class="text-dark text-uppercase d-inline-block">
                                                <span class="font-weight-semibold text-dark">Date :</span>
                                            </div>
                                            <div class="float-right">{{ $data->tour_date}}</div>
                                        </div>
                                    </div>
                                    <h3 style="text-align: center;">Prize Pool</h3>
                                    <div class="m-t-20">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Standings</th>                                                        
                                                        <th>Price</th>                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($GetPricePool as $_data)
                                                    <tr>
                                                        <th>{{ $_data->standing_no}}</th>                                                        
                                                        <td>₹{{ $_data->price_pool}}</td>                                                        
                                                    </tr>
                                                     @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                        @if (Session::has('message'))
                                           <div class="alert alert-success" role="alert">
                                               {{Session::get('message')}}
                                           </div>
                                        @endif
                                        <div class="form-group row">
                                            <form method="POST" action="/addprice">
                                                {{ csrf_field() }}
                                                <div class="col-sm-6">
                                                    <div class="input-group-prepend">
                                                        <input type="text" value="{{ $data->tournament_id}}" name="tour_id" class="form-control" readonly="">&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <span class="input-group-text">No</span>
                                                        <input type="text" class="form-control" name="standing_no" placeholder="No.">
                                                            <span class="input-group-text">₹</span>
                                                        <input type="text" class="form-control" name="price_pool" placeholder="Price Pool">&nbsp;
                                                        <button class="btn btn-primary">Add</button>
                                                    </div>                                                                                     
                                                </div> 
                                            </form>
                                        </div>
                                                                    
                                    <h3 style="text-align: center;">Result</h3>
                                    <div class="m-t-20">
                                        <div class="table-responsive">
                                            <table class="table">
                                                
                                                <thead>
                                                    <tr>
                                                        <th>Standings</th>                                                        
                                                        <th>Player</th>                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th>1</th>                                                        
                                                        <td>120</td>                                                        
                                                    </tr>
                                                </tbody>
                                                
                                            </table>
                                        </div>
                                    </div>
                                    @if (Session::has('message'))
                                           <div class="alert alert-success" role="alert">
                                               {{Session::get('message')}}
                                           </div>
                                        @endif
                                        <div class="form-group row">
                                            <form method="POST" action="/addresult">
                                                {{ csrf_field() }}
                                                <div class="col-sm-8">
                                                    <div class="input-group-prepend">
                                                        <input type="text" value="{{ $data->tournament_id}}" name="tour_id" class="form-control" readonly="">&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <span class="input-group-text">No</span>
                                                        <input type="text" class="form-control" name="standing_no" placeholder="No.">
                                                            <span class="input-group-text">Name</span>
                                                        <input type="text" class="form-control" name="price_pool" placeholder="Player Name">&nbsp;
                                                        <button class="btn btn-primary">Add</button>
                                                    </div>                                                                                     
                                                </div> 
                                            </form>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- Content Wrapper END -->

                <!-- Footer START -->
                <footer class="footer">
                    <div class="footer-content">
                        <p class="m-b-0">Copyright © 2019 Theme_Nate. All rights reserved.</p>
                        <span>
                            <a href="#" class="text-gray m-r-15">Term &amp; Conditions</a>
                            <a href="#" class="text-gray">Privacy &amp; Policy</a>
                        </span>
                    </div>
                </footer>
                <!-- Footer END -->

            </div>
@stop