@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="offset-md-4 col-md-4">
            <h1 style="color: white;">Registration Details</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 offset-md-2 checkout-left-box">
            <div class="alert alert-danger" id="err" role="alert" style="display: none;"></div>

            <form method="post" action="/registration" class="needs-validation">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="pubgID" class="col-form-label">Leader PUBG ID</label>
                        <input type="number" class="form-control" id="leaderPubgId" name="leaderPubgId" value="5298730377" required="" />
                        <div class="invalid-feedback" id="leaderPubgIdFeedback"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="leaderName" class="col-form-label">Leader PUBG In-Game NAME <span class="spinner-border spinner-border-sm spinner" style="display: none;" role="status" aria-hidden="true"></span></label>

                        <input type="text" class="form-control" id="leaderName" name="leaderName" value="T4T Rv" readonly="" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="leaderNumber" class="col-form-label">Leader's Whatsapp Number</label>
                    <input type="number" class="form-control" id="leaderNumber" name="leaderNumber" value="8849569500" required="" />
                    <div class="invalid-feedback" id="numberFeedback"></div>
                </div>
                <br />

                <p style="color: #d9603f;">PUBG Mobile in-game name and Game ID submitted on DaddyDope should match or else the player won't receive any reward.</p>
                <p style="color: #d9603f;">If we caught you sharing Room ID and Password with other players who are not registered with us . Your Daddydope account will be banned permanently.</p>

                <div class="form-row">
                    <button type="submit" class="fag-btn">Register Now</button>
                </div>
                <br />
                <div class="form-row">
                    <button class="fag-btn" id="back">Back</button>
                </div>

                <!-- <button type="submit" class="fag-btn">Register Now</button> -->
            </form>
        </div>
    </div>
</div>
@stop