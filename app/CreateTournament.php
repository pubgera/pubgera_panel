<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;


class CreateTournament extends Authenticatable
{
    use Notifiable;


    protected $table = "create_tournament";
    public $timestamps = false;
    public $timezone = 'Asia/Kolkata';



    public function addTournament($tournament_id, $tour_name, $tour_type, $tour_map, $tour_date, $tour_price_pool, $tour_player ,$tour_entry_fee){

        try{
            
            $this->tournament_id = $tournament_id;
            $this->tour_name = $tour_name;
            $this->tour_type = $tour_type;
            $this->tour_map = $tour_map;
            $this->tour_date = $tour_date;
            $this->tour_price_pool = $tour_price_pool;
            $this->tour_player = $tour_player;
            $this->tour_entry_fee = $tour_entry_fee;

            if($this->save()) {
                return true;
            }
            return null;

            } catch (QueryExeption $ex){

                Log::info($ex->getMessage());
                return null;
            }

    }

    public function getAllTour()
    {
        try {

            $TourList = $this->orderBy('timestamp', 'desc')->get();
            
            
            if($TourList) {
                return $TourList;
            }
            return null;
        } catch(QueryException $queryException) {
            Log::error('User Model', ['getAllUsers' => $queryException->getMessage()]);
            return null;
        }
    }

    public function getTourDetails($tournament_id)
    {
        try {

            $GetTourDetails = $this->where('tournament_id', $tournament_id)->get();
            
            
            if($GetTourDetails) {
                return $GetTourDetails;
            }
            return null;
        } catch(QueryException $queryException) {
            Log::error('User Model', ['getAllUsers' => $queryException->getMessage()]);
            return null;
        }
    }

}
