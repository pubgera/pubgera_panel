<?php

namespace App\Classes;


use App\User;
use App\CreateTournament;

use Illuminate\Support\Facades\Log;


class TournamentManager
{
	protected $user;
    protected $CreateTournament;
    
	public function __construct(User $user, CreateTournament $CreateTournament) {
        $this->user = $user;
        $this->CreateTournament = $CreateTournament;
    }

    public function getAllUsers()
    {
    	try {
    		$userList = $this->user->getAllUsers();

    		if($userList) {
    			return $userList;
    		}
    		return null;
    	} catch(\Exception $exception) {
    		Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
    		Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
    	}
    }

    public function getAllTour()
    {
        try {
            $TourList = $this->CreateTournament->getAllTour();

            if($TourList) {
                return $TourList;
            }
            return null;
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
        }
    }

    public function getTourDetails($tournament_id)
    {
        try {
            $GetTourDetails = $this->CreateTournament->getTourDetails($tournament_id);

            if($GetTourDetails) {
                return $GetTourDetails;
            }
            return null;
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
        }
    }

    public function getUserBankRejected()
    {
        try {
            $userList = $this->user->getUserBankRejected();

            if($userList) {
                return $userList;
            }
            return null;
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
        }
    }
    public function getUserBankNotAvailable()
    {
        try {
            $userList = $this->user->getUserBankNotAvailable();

            if($userList) {
                return $userList;
            }
            return null;
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
        }
    }

    public function getUserWithBankPending()
    {
        try {
            $userList = $this->user->getUserWithBankPending();

            if($userList) {
                return $userList;
            }
            return null;
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
        }
    }

    public function getUserWithKycPending()
    {
        try {
            $userList = $this->user->getUserWithKycPending();

            if($userList) {
                return $userList;
            }
            return null;
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
        }
    }

    public function getUserTodayKyc()
    {
        try {
            $userList = $this->user->getUserTodayKyc();

            if($userList) {
                return $userList;
            }
            return null;
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
        }
    }

    public function searchUser($inputData)
    {
        try {
            $pageId = $inputData['pageId'];
            Paginator::currentPageResolver(function () use ($pageId) {
                return $pageId;
            });

            $searchUser = $this->user->searchUser($inputData);
            return view('structures.user.filter_user_table')->with('searchUser', $searchUser);
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['searchUser' => $exception->getMessage()]);
            Log::error('userManager Error', ['searchUser' => $exception->getMessage()]);
            $result['status'] = false;
            $result['data']['message'] = trans('user.error_while_getting_user');
            return prepareRes($result, 500);
        }
    }

    public function getUserDetails($mobNo)
    {
        try {
            $userDetails['bank_details'] = $this->bankDetail->getBankDetails($mobNo);
            $userDetails['mob_no'] = $mobNo;
            
            if($userDetails) {
                return $userDetails;
            }
            return null;
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['getUserDetails' => $exception->getMessage()]);
            Log::error('userManager Error', ['getUserDetails' => $exception->getMessage()]);
            return null;
        }
    }

    public function setDefaultBank($bankId)
    {
        try {
            $setDefaultBank = $this->bankDetail->setDefaultBank($bankId);

            if($setDefaultBank) {
                $result['status'] = true;
                $result['message'] = trans('user.default_bank_set_success');
                return prepareRes($result, 200);
            }
            $result['status'] = false;
            $result['message'] = trans('user.default_bank_set_failed');
            return prepareRes($result, 400);
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['setDefaultBank' => $exception->getMessage()]);
            Log::error('userManager Error', ['setDefaultBank' => $exception->getMessage()]);
            $result['status'] = false;
            $result['message'] = trans('user.default_bank_set_failed');
            return prepareRes($result, 500);
        }
    }

    public function saveNewBank($inputData)
    {
        try {
            $saveNewBank = $this->bankDetail->saveNewBank($inputData);

            if($saveNewBank) {
                $result['status'] = true;
                $result['message'] = trans('user.new_bank_added_success');
                return prepareRes($result, 200);
            }
            $result['status'] = false;
            $result['message'] = trans('user.new_bank_added_failed');
            return prepareRes($result, 400);
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['saveNewBank' => $exception->getMessage()]);
            Log::error('userManager Error', ['saveNewBank' => $exception->getMessage()]);
            $result['status'] = false;
            $result['message'] = trans('user.new_bank_added_failed');
            return prepareRes($result, 500);
        }
    }

    public function changePassword($userId, $currentPassword, $newPassword)
    {
        if ($this->adminUser->changePassword($userId, $currentPassword, $newPassword, $message)) {
            $result['status'] = true;
            $result['message'] = $message;
            return prepareRes($result, 200);
        }

        $result['status'] = false;
        $result['message'] = $message;
        return prepareRes($result, 400);
    }

    public function suspendUser($inputData)
    {
        try {
            $setDefaultBank = $this->user->suspendUser($inputData);

            if($setDefaultBank) {
                $result['status'] = true;
                $result['message'] = trans('user.user_suspended_success');
                return prepareRes($result, 200);
            }
            $result['status'] = false;
            $result['message'] = trans('user.user_suspended_failed');
            return prepareRes($result, 400);
        } catch(\Exception $exception) {
            Log::critical('userManager Error', ['suspendUser' => $exception->getMessage()]);
            Log::error('userManager Error', ['suspendUser' => $exception->getMessage()]);
            $result['status'] = false;
            $result['message'] = trans('user.user_suspended_failed');
            return prepareRes($result, 500);
        }
    }
}