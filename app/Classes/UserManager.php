<?php

namespace App\Classes;


use App\User;
use Illuminate\Support\Facades\Log;

class UserManager
{
	protected $user;
    
	public function __construct(User $user) {
        $this->user = $user;
      
    }

    public function getAllUsers()
    {
    	try {

    		$userList = $this->user->getAllUsers();
               
                
    		if($userList) {
    			return $userList;
    		}
    		return null;
    	} catch(\Exception $exception) {
    		Log::critical('userManager Error', ['getAllUsers' => $exception->getMessage()]);
    		Log::error('userManager Error', ['getAllUsers' => $exception->getMessage()]);
            return null;
    	}
    }

}