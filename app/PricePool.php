<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;


class PricePool extends Authenticatable
{
    use Notifiable;


    protected $table = "tbl_price_pool";
    public $timestamps = false;
    public $timezone = 'Asia/Kolkata';



    public function addPricePool($tour_id, $standing_no, $price_pool){

        try{
            
            $this->tour_id = $tour_id;
            $this->standing_no = $standing_no;
            $this->price_pool = $price_pool;

            if($this->save()) {
                return true;
            }
            return null;

            } catch (QueryExeption $ex){

                Log::info($ex->getMessage());
                return null;
            }

    }

    public function getPricePool($tournament_id)
    {
        try {

            $getPricePool = $this->where('tour_id', $tournament_id)->get();
            
            
            if($getPricePool) {
                return $getPricePool;
            }
            return null;
        } catch(QueryException $queryException) {
            Log::error('PricePool Model', ['getPricePool' => $queryException->getMessage()]);
            return null;
        }
    }

}
