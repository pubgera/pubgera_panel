<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Classes\logActionManager;



class User extends Model
{

    protected $connection = 'mysql';
    protected $table = 'users';
    public $timestamps = false;

    public function getAllUsers()
    {
        try {

            $userList = $this->orderBy('created_at', 'desc')->get();
            
            
            if($userList) {
                return $userList;
            }
            return null;
        } catch(QueryException $queryException) {
            Log::error('User Model', ['getAllUsers' => $queryException->getMessage()]);
            return null;
        }
    }

}
