<?php

namespace App\Http\Controllers;

use App\CreateTournament;
use App\PricePool;
use App\Classes\TournamentManager;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class TournamentController extends Controller
{
    
    private $CreateTournament;
    private $TournamentManager;
    private $PricePool;

    public function __construct(CreateTournament $CreateTournament, TournamentManager $TournamentManager, PricePool $PricePool)
    {
        $this->CreateTournament = $CreateTournament;
        $this->TournamentManager = $TournamentManager;
        $this->PricePool = $PricePool;

    }

    public function getAllTour()
    {
        
        $TourList = $this->TournamentManager->getAllTour();

        return view('pages.TourList', compact('TourList'));
    }

    public function addTournament(Request $request)
    {


    	$tournament_id = "Tour" .rand(11111,99999);
    	$tour_name = $request->get('tour_name');
        $tour_type = $request->get('tour_type');
        $tour_map = $request->get('tour_map');
    	$tour_date = $request->get('tour_date');
    	$tour_price = $request->get('tour_price_pool');
    	$tour_player = $request->get('tour_player');
        $tour_entry_fee = $request->get('tour_entry_fee');

    	try{

    		$this->CreateTournament->addTournament($tournament_id, $tour_name, $tour_type, $tour_map, $tour_date, $tour_price, $tour_player ,$tour_entry_fee);

    	} catch (QueryExeption $ex){

                Log::info($ex->getMessage());
                return null;
            }

    	return back()->with('message', 'Create Tournament Successfully'); 

    }
    public function getTourDetails($tournament_id)
    {
        
        $GetTourDetails = $this->TournamentManager->getTourDetails($tournament_id);
        $GetPricePool = $this->PricePool->GetPricePool($tournament_id);
        
        return view('pages.TourEdit', compact('GetTourDetails','GetPricePool'));
    }

    public function addPricePool(Request $request)
    {

        $tour_id = $request->get('tour_id');
        $standing_no = $request->get('standing_no');
        $price_pool = $request->get('price_pool');
        

            $this->PricePool->addPricePool($tour_id, $standing_no, $price_pool);
        
        return back()->with('message', 'Add Price Pool Successfully');
    }
}
