<?php

namespace App\Http\Controllers;

use App\Classes\UserManager;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class UserController extends Controller
{
    
    private $UserManager;

    public function __construct(UserManager $UserManager)
    {
        $this->UserManager = $UserManager;

    }

    public function getAllUsers()
    {
        
        $userList = $this->UserManager->getAllUsers();

        return view('pages.Users', compact('userList'));
    }
}
